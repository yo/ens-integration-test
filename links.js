const fetch = require("node-fetch")

fetch(
  "https://raw.githubusercontent.com/ensdomains/ensdomains-v2/master/src/components/Home/links.json"
)
  .then(res => res.json())
  .then(res => {
    for (const app of Object.values(res)) {
      console.log(app.link)
    }
  })
